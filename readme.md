# Diploradar

This Lumen Project is a set of commands for scraping data about diplomatic events from websites of foreign ministries and
preparing it to be used to train an Entity Extraction and Document Classification ML Model.

The crawling is built upon the [spatie/crawler](https://github.com/spatie/crawler) package.